package com.qf.student.common.base.ex;

import com.qf.student.common.base.result.ResultStatus;

public class ServiceException extends BaseException{
    public ServiceException(ResultStatus resultStatus) {
        super(resultStatus);
    }
}
