package com.qf.student.common.base.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 获取异常信息类
 * @author
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ArgsErrorInfo {
    private String errorMessage;
    private String fileName;
}
