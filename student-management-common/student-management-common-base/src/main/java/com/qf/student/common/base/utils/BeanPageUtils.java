package com.qf.student.common.base.utils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.util.function.Supplier;
/**
 * Mybatis Plus  Page对象转化
 * Page<S> source ---> Page<Member>  source.getRecords()    ---->List<Member>
 * Page<T> target ----> Page<MemberVo>
 *
 */
public class BeanPageUtils extends CommonBeanUtils {
    public static <S, T> Page<T> convertPage(Page<S> source, Page<T> target, Supplier<T> targetSupplier) {
        /*
         * 先复制外层 page对象
         */
        copyProperties(source, target);
        List<T> ts = convertListTo(source.getRecords(), targetSupplier);
        target.setRecords(ts);
        return target;
    }
}
