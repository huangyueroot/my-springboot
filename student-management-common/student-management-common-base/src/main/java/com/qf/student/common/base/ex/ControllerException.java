package com.qf.student.common.base.ex;

import com.qf.student.common.base.result.ResultStatus;

public class ControllerException extends BaseException{
    public ControllerException(ResultStatus resultStatus) {
        super(resultStatus);
    }
}
