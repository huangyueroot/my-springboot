package com.qf.student.common.base.ex;

import com.qf.student.common.base.result.ResultStatus;
import lombok.Getter;

@Getter
//getter是因为要返回给前端resultStatus结果集对象，所以要给个getter
public class BaseException extends RuntimeException{
    private final ResultStatus resultStatus;

    public BaseException(ResultStatus resultStatus) {
        //为了让他在控制台打印错误信息
        super(resultStatus.getMsg());
        this.resultStatus = resultStatus;
    }
}
