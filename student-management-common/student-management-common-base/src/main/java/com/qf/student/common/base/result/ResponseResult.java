package com.qf.student.common.base.result;

import lombok.Data;

@Data
public class ResponseResult<T> {
    private int code;
    private String msg;
    private T data;

    public static <T> ResponseResult<T> success(T data) {
        return success(ResultStatus.SUCCESS,data);
    }

    public static <T> ResponseResult<T> success(ResultStatus resultStatus, T data) {
        return common(resultStatus, data);
    }

    public static <T> ResponseResult<T> error(ResultStatus resultStatus) {
        return common(resultStatus, null);
    }

    //    传递错误信息到前端
    public static <T> ResponseResult<T> error(ResultStatus resultStatus,T errorData){
        return common(resultStatus, errorData);
    }

    public static <T> ResponseResult<T> error() {
        return error(ResultStatus.SYS_ERROR);
    }

    private static <T> ResponseResult<T> common(ResultStatus resultStatus, T data) {
        ResponseResult<T> result = new ResponseResult<>();
        result.setCode(resultStatus.getCode());
        result.setMsg(resultStatus.getMsg());
        result.setData(data);
        return result;
    }

}
