package com.qf.student.common.base.handler;
import com.qf.student.common.base.result.ResponseResult;
import com.qf.student.common.base.ex.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 *
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseResult<Object> handlerException(Exception ex) {
        log.error(ex.getMessage());
        return ResponseResult.error();
    }

    @ExceptionHandler(BaseException.class)
    public ResponseResult<Object> handlerException(BaseException ex) {
        return ResponseResult.error(ex.getResultStatus());
    }
}
