package com.qf.student.common.base.utils;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class CommonBeanUtils extends BeanUtils {

    /**
     * 将A对象的属性赋值给B对象
     * @param source
     * @param targetSupplier
     * @return
     * @param <S>
     * @param <T>
     */
    public static <S,T> T convertTo(S source, Supplier<T> targetSupplier){
        //        这里的ObjectUtils.isEmpty(targetSupplier)是为了方式下面的get出现空指针异常
        if (ObjectUtils.isEmpty(source) || ObjectUtils.isEmpty(targetSupplier)){
            return null;
        }
        T t = targetSupplier.get();
        BeanUtils.copyProperties(source,t);
        return t;
    }

    /**
     * 调用convertTo(s, targetSupplier)方法来生成一个新的T类型对象。
     * 然后使用collect(Collectors.toList())将所有这些新生成的T类型对象收集到一个新的列表中
     * @param sources
     * @param targetSupplier
     * @return
     * @param <S>
     * @param <T>
     */
    public static <S,T> List<T> convertListTo(List<S> sources, Supplier<T> targetSupplier){
        if (ObjectUtils.isEmpty(sources) || ObjectUtils.isEmpty(targetSupplier)){
            return null;
        }

        //把每个对象赋值属性然后用collect转为集合
        return sources.stream().map(s -> convertTo(s,targetSupplier)).collect(Collectors.toList());
    }

    /**
     * 处理分页查询时，当你想要将一个分页查询的结果转换为另一种类型的分页查询结果时。
     * 例如，你可能有一个基于User的分页查询，并且想要将其转换为基于Employee的分页查询
     * @param source
     * @param target
     * @param targetSupplier
     * @return
     * @param <S>
     * @param <T>
     */
    public static <S,T> PageInfo<T> convertPageInfo(PageInfo<S> source, PageInfo<T> target, Supplier<T> targetSupplier){
        //属性复制
        copyProperties(source,target);
        //将source对象的列表转换为target对象的列表。
        // source.getList()返回source对象的列表，
        // 而targetSupplier是一个函数式接口，用于生成新的目标对象实例。
        target.setList(convertListTo(source.getList(),targetSupplier));
        return target;
    }

}
