package com.qf.student.common.base.result;

import lombok.Getter;

@Getter
@SuppressWarnings("all")
public enum ResultStatus {
    SUCCESS(20000, "成功"),
    SYS_ERROR(40000, "系统错误"),
    DELETE_FAULT(406, "删除失败"),
    USER_PARAM_ERROR(40001, "用户名格式不正确"),
    INSERT_FAULT(406, "添加失败"),
    USER_NO_EXIST(40002, "用户名不存在"),
    UPDATE_FAULT(44444,"更新失败"),
    USER_PASSWORD_ERROR(40003, "密码错误"),
    QUERY_EMPTY(00011,"查询异常"),
    USER_LOGIN_ERROR (40004, "用户名或者密码错误"),

    USER_ACCOUNT_ERROR (40005, "账号被禁用"),
    PICTURE_UPLOAD_ERROR (40006, "图片上传异常"),
    PICTURE_DOWNLOAD_ERROR (40007, "图片下载异常"),


    ARGS_VALID_ERROR (40008, "参数校验失败");


    final int code;
    final String msg;

    ResultStatus(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
