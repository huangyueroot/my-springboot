package com.smart.student.common.swagger.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @author Mr.zhang
 */
@Component
@ConfigurationProperties("swagger")
@Data
public class SwaggerProperties {
    private String title;
    private String basePackage;
    private String groupName;
    private String description;

    private String name;
    private String url;
    private String email;

    private String version;

}